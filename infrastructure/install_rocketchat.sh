#! /bin/bash 

# Install Docker user get docker script
curl -o docker.sh https://get.docker.com/
chmod +x docker.sh
./docker.sh -y

# Install Docker Compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

# Grab Docker Compose
curl -o ./docker-compose.yml https://gitlab.com/mastermnd/shakemates/-/raw/master/infrastructure/docker-compose/docker-compose.yaml 

#launch app
docker-compose up -d

echo "done"